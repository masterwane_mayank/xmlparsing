//
//  Student.swift
//  XMLParsing
//
//  Created by Mayank Rawat on 11/01/19.
//  Copyright © 2019 Mayank Rawat. All rights reserved.
//

import Foundation
struct Student {
    var name: String?
    var job: String?
    var location: String?
    init(name: String, job: String, location: String) {
        self.name = name
        self.job = job
        self.location = location
    }
}
