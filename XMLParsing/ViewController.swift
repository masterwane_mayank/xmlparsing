//
//  ViewController.swift
//  XMLParsing
//
//  Created by Mayank Rawat on 11/01/19.
//  Copyright © 2019 Mayank Rawat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, XMLParserDelegate {
    var students = [Student]()
    var student = Student(name: "mayank", job: "noida", location: "ncr")
    //var tempStudent: Student?
    var eName: String?
    var name = ""
    var job = ""
    var location = ""
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        if let path = Bundle.main.url(forResource: "Books", withExtension: "xml") {
            if let parser = XMLParser(contentsOf: path) {
                parser.delegate = self
                parser.parse()
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName == "student") {
//            let stu = Student()
//            stu.name = name
//            stu.job = job
//            stu.location = location
//            students.append(stu)
        }
        if (elementName == "student") {
            students.append(student)
            
        }
    }
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.eName = elementName
        if(eName == "student") {
            student.name = ""
            student.location = ""
            self.job = ""
        }
        
    }
    func parser(_ parser: XMLParser, foundCharacters string: String) {
       let data = string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
//        if(!data.isEmpty) {
//            if(eName == "name") {
//                self.name += data
//            } else if(eName == "job") {
//                self.job += data
//            } else if(eName == "location"){
//                self.location += data
//            }
//        }
        if(!data.isEmpty) {
                        if(eName == "name") {
                           student.name = data
                        } else if(eName == "job") {
                            student.job = data
                        } else if(eName == "location"){
                           student.location = data
                        }
                    }
    }
}
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.students.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "studentCell", for: indexPath) as? StudentCell  {
            cell.nameLabel.text = students[indexPath.item].name
            cell.jobLabel.text = students[indexPath.item].job
            cell.locationLabel.text = students[indexPath.item].location
            return cell
        }
       return UICollectionViewCell()
    }
    
    
}
class StudentCell: UICollectionViewCell {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var studentImage: UIImageView!
}
